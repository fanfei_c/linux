#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <cerrno>
#include <cstdlib>

#include "Log.hpp"
#include "LockGuard.hpp"
#include "InetAddr.hpp"

// 网络套接字必用头文件
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

enum
{
  SOCKET_ERROR = 1,
  BIND_ERROR,
  USAGE_ERROR
};

const static int sockfddefault = -1;

class UdpServer
{
private:
  int _sockfd;
  uint16_t _port;  // 服务端所用端口号
  bool _isrunning; // 运行位

public:
  UdpServer(uint16_t port) : _sockfd(sockfddefault), _port(port), _isrunning(false) {}
  void InitServer()
  {
    // 1.创建UDP套接字
    _sockfd = socket(AF_INET, SOCK_DGRAM, 0); // 返回文件描述符
    if (_sockfd < 0)
    {
      LOG(FATAL, "socket error,%s,%d", strerror(errno), errno);
      exit(SOCKET_ERROR);
    }
    LOG(INFO, "socket create success,sockfd:%d", _sockfd);

    // 2.0填充sockaddr_in结构
    struct sockaddr_in local;      // struct sockaddr_in 系统提供的数据类型。local是变量，用户栈上开辟空间。
    bzero(&local, sizeof(local));  // 将从&local开始的sizeof(local)大小的内存区域置零
    local.sin_family = AF_INET;    // 设置网络通信方式
    local.sin_port = htons(_port); // port要经过网络传输给对面，所有需要从主机序列转换为网络序列
    // a. 字符串风格的点分十进制的IP地址转成 4 字节IP
    // b. 主机序列，转成网络序列
    // in_addr_t inet_addr(const char *cp) -> 该函数可以同时完成 a & b
    // local.sin_addr.s_addr = inet_addr(_ip.c_str()); // "192.168.3.1" -> 字符串风格的点分十进制的IP地址 -> 4字节IP
    local.sin_addr.s_addr = INADDR_ANY; // INADDR_ANY宏的值为0，给local.sin_addr.s_addr设置为0代表任意IP，因为一个服务器有多个IP，为了确保所有请求_port端口的请求都能得到相应，所以设置为0
    // 2.1bind绑定sockfd和网络信息(IP+PORT)
    int n = bind(_sockfd, (struct sockaddr *)&local, sizeof(local));
    if (n < 0)
    {
      LOG(FATAL, "bind error,%s,%d", strerror(errno), errno);
      exit(BIND_ERROR);
    }
    LOG(INFO, "socket bind success");
  }
  void Start()
  {
    // 一直运行，直到管理者不想运行了， 服务器都是死循环
    // UDP是面向数据报的协议
    _isrunning = true;
    while (true)
    {
      char buffer[1024];
      struct sockaddr_in peer;
      socklen_t len = sizeof(peer); // 必须初始化为sizeof(peer)，不能是0
      // 1.要先让server接收数据
      ssize_t n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
      if (n > 0)
      {
        buffer[n] = 0;
        InetAddr addr(peer);
        LOG(DEBUG, "get message from [%s:%d]: %s\n", addr.Ip().c_str(), addr.Port(), buffer);
        // 2. 我们要将server收到的数据，发回给对方
        sendto(_sockfd, buffer, strlen(buffer), 0, (struct sockaddr *)&peer, len);
      }
    }
    _isrunning = false;
  }
  ~UdpServer() {}
};
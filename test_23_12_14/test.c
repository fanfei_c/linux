#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<stdlib.h>
int main()
{
    printf("I am a process,pid:%d\n",getpid());
    char *const env[]={
        (char*)"PATH=/",
        (char*)"value=a"
    };
    pid_t id =fork();
    if(id==0)
    {
        execle("./mytest","mytest",NULL,env);
        exit(1);
    }
    pid_t rid=waitpid(id,NULL,0);
    if(rid>0)
    {
        printf("wait success\n");
    }
    exit(1);
    return 0;
}
//int main()
//{
//    pid_t id=fork();
//    if(id==0)
//    {
//        int cnt=5;
//        while(cnt)
//        {
//            printf("I am child process,pid:%d,ppid:%d,cnt:%d\n",getpid(),getppid(),cnt);
//            cnt--;
//            sleep(1);
//        }
//        sleep(1);
//        exit(0);
//    }
//    int status=0;
//    while(1)
//    {
//        pid_t rid=waitpid(id,&status,WNOHANG); 
//        if(rid > 0)
//        {
//            printf("wait success,rid:%d,status:%d,exitcode:%d\n",rid,status,WEXITSTATUS(status));
//            break;
//        }
//        else if(rid == 0)
//        {
//            printf("child is running,father do other thing\n");
//        }
//        else
//        {
//            perror("waitpid:");
//            break;
//        }
//        sleep(1);
//    }
//    return 0;
//}

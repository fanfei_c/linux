#include <iostream>
#include <unistd.h>

using namespace std;

int main(int argc,char* argv[],char* env[])
{
    for(int i = 0; env[i]; i++)
    {
        printf("env[%d]: %s\n", i, env[i]);
    }

    cout << "hello C++" << endl;
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h> //线程所用头函数
/*定义线程的执行函数*/
void thread(void) // 必须使用void作为返回类型
{
  int i;
  for (i = 0; i < 3; i++)
  {
    printf("This is the second pthread.\n");
    // 显示自己是子线程
    sleep(1);
  }
}
int main()
{ /*定义线程内部标识 */
  pthread_t threadid;
  int i, ret;
  /*创建一个子线程并指定执行函数，函数不带参数*/
  ret = pthread_create(&threadid, NULL, (void *)thread, NULL);
  if (ret != 0)
  {
    printf("Create pthread error!\n");
    exit(1);
  }
  /*主线程循环输出3次*/
  pthread_join(threadid, NULL); // 等待子线程结束
  for (i = 0; i < 3; i++)
  {
    printf("This is the main pthread.\n");
    sleep(1);
  }
  exit(0);
}

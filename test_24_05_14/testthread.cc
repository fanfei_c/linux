#include <iostream>
#include <unistd.h>
#include <pthread.h>

int tickets = 1000;
void *route(void *arg)
{
  const char *name = (char *)arg;
  while (1)
  {
    if (tickets > 0)
    {
      usleep(10000);
      std::cout << name << " get a ticket, remain: " << --tickets << std::endl;
    }
    else
    {
      break;
    }
  }
  std::cout << name << "quit!" << std::endl;
  pthread_exit((void *)0);
}

int main()
{
  pthread_t t1, t2, t3, t4;
  pthread_create(&t1, NULL, route, (void *)"thread 1");
  pthread_create(&t2, NULL, route, (void *)"thread 2");
  pthread_create(&t3, NULL, route, (void *)"thread 3");
  pthread_create(&t4, NULL, route, (void *)"thread 4");

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  pthread_join(t3, NULL);
  pthread_join(t4, NULL);
  return 0;
}
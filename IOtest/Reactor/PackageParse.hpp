#pragma once

#include <iostream>
#include "Connection.hpp"
#include "Protocol.hpp"
#include "Calculate.hpp"
using namespace protocol_ns;

class PackageParse
{
private:
public:
    static void Parse(Connection *conn)
    {
        // std::cout << "inbuffer: " << conn->Inbuffer() << std::endl;
        // 2.分析数据，获取完整报文
        std::string package;
        Request req;
        Calculate cal;
        while (true)
        {
            package = Decode(conn->Inbuffer());
            if (package.empty())
                break; // 证明此时没有一个完整的报文继续读取，这也是为什么Socket类中Recv接口中*out+=inbuffer;是+=的原因
            // 代码执行到这一定有完整json字符串
            std::cout << "----------------------begin----------------------" << std::endl;
            std::cout << "请求json字符串:\n"
                      << package << std::endl;
            // 3.反序列化
            req.Deserialize(package);
            // 4.业务处理
            Response resp = cal.Excute(req);
            // 5.对响应序列化
            std::string send_str;
            resp.Serialize(&send_str);

            std::cout << "响应序列化:" << std::endl;
            std::cout << send_str << std::endl;

            // 6.添加长度报头
            send_str = Encode(send_str);
            std::cout << "响应完整报文:" << std::endl;
            std::cout << send_str << std::endl;

            conn->AppendOutBuffer(send_str);
        }
        if (!conn->OutBufferEmpty() && conn->Sender() != nullptr)
        {
            // 以下两种发送方式
            conn->Sender()(conn);
            // conn->R()->EnableReadWrite(conn->Sockfd(),true,true); //开启对写事件关心也可以写入
        }
    }
};
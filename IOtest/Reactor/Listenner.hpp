// 连接管理模块
#pragma once

#include <iostream>
#include <memory>
#include "Epoller.hpp"
#include "Socket.hpp"
#include "InetAddr.hpp"
#include "Connection.hpp"
#include "HandlerConnection.hpp"

using namespace socket_ns;

class Listenner
{
private:
    uint16_t _port;
    std::unique_ptr<Socket> _listensock;
    HandlerConnection &_hc;

public:
    Listenner(int port, HandlerConnection &hc)
        : _port(port),
          _listensock(std::make_unique<TcpSocket>()),
          _hc(hc)
    {
        InetAddr addr("0", port);
        _listensock->BuildListenSocket(addr);
    }
    void Accepter(Connection *conn)
    {
        while (true)
        {
            InetAddr clientaddr;
            int code = 0;
            int sockfd = _listensock->Accepter(&clientaddr, &code);
            if (sockfd >= 0)
            {
                // 获取到新连接后需要将新连接交给Reactor中的Epoller管理，这时候就需要用到Connection对象中的R指针了
                conn->R()->AddConnection(sockfd, EPOLLIN | EPOLLET,
                                         std::bind(&HandlerConnection::HanlderRecv, &_hc, std::placeholders::_1),
                                         std::bind(&HandlerConnection::HanlderSend, &_hc, std::placeholders::_1),
                                         std::bind(&HandlerConnection::HanlderExcpet, &_hc, std::placeholders::_1));
            }
            else
            {
                if (code == EWOULDBLOCK || code == EAGAIN)
                {
                    LOG(DEBUG, "accept all link");
                    break;
                }
                else if (code == EINTR)
                {
                    LOG(DEBUG, "accepter interupt by signal");
                    continue;
                }
                else
                {
                    LOG(WARNING, "accept error");
                    break;
                }
            }
        }
    }
    int Sockfd()
    {
        return _listensock->SockFd();
    }

    ~Listenner()
    {
        _listensock->Close();
    }
};

#pragma once

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

// 这是一个可以获取点分十进制格式IP地址和Port端口号的类
class InetAddr
{
private:
    struct sockaddr_in _addr;
    std::string _ip;
    uint16_t _port;

private:
    void GetAddress(std::string *ip, uint16_t *port)
    {
        *port = ntohs(_addr.sin_port);
        *ip = inet_ntoa(_addr.sin_addr); // inet_ntoa是一个用于将网络字节序的 IP 地址转换为点分十进制的字符串格式（如 "192.168.1.1"）的函数
    }

public:
    InetAddr(const struct sockaddr_in &addr) : _addr(addr)
    {
        GetAddress(&_ip, &_port);
    }
    InetAddr(const std::string &ip, uint16_t port) : _ip(ip), _port(port)
    {
        _addr.sin_family = AF_INET;
        _addr.sin_port = htons(_port);
        _addr.sin_addr.s_addr = inet_addr(ip.c_str());
    }
    InetAddr()
    {
    }
    std::string Ip()
    {
        return _ip;
    }
    uint16_t Port()
    {
        return _port;
    }
    bool operator==(const InetAddr &addr)
    {
        if (_ip == addr._ip && _port == addr._port)
        {
            return true;
        }
        return false;
    }
    struct sockaddr_in Addr()
    {
        return _addr;
    }
    ~InetAddr() {}
};
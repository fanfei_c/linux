#pragma once

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <functional>
#include "Log.hpp"
#include "Connection.hpp"

class HandlerConnection
{
public:
    HandlerConnection(func_t func) : _func(func)
    {
    }
    void HanlderRecv(Connection *conn)
    {
        errno = 0;
        LOG(DEBUG, "HanlderRecv fd : %d\n", conn->Sockfd());
        while (true)
        {
            char buffer[1024];
            ssize_t n = ::recv(conn->Sockfd(), buffer, sizeof(buffer) - 1, 0);
            if (n > 0)
            {
                buffer[n] = 0;
                conn->AppendInBuffer(buffer);
            }
            else
            {
                if (errno == EWOULDBLOCK || errno == EAGAIN)
                {
                    break;
                }
                else if (errno == EINTR)
                {
                    continue;
                }
                else
                {
                    conn->Excepter()(conn);
                    return; // 异常处理完后一定要提前返回
                }
            }
        }
        _func(conn);
    }
    void HanlderSend(Connection *conn)
    {
        errno = 0;
        while (true)
        {
            ssize_t n = ::send(conn->Sockfd(), conn->Outbuffer().c_str(), conn->Outbuffer().size(), 0);
            // 以下是发送完毕的情况
            if (n > 0)
            {
                conn->OutBufferRemove(n);
                if (conn->OutBufferEmpty())
                    break;
            }
            else if (n == 0)
                break;
            // 以下是没发送完的情况
            else
            {
                if (errno == EWOULDBLOCK || errno == EAGAIN)
                {
                    break; // 这里代表缓冲区被写满了
                }
                else if (errno == EINTR)
                {
                    continue;
                }
                else
                {
                    conn->Excepter()(conn);
                    return;
                }
            }
        }
        // 一定遇到了 ，发送缓冲区被写满的情况
        if (!conn->OutBufferEmpty())
        {
            conn->R()->EnableReadWrite(conn->Sockfd(), true, true); // 开启对写事件的关心
        }
        else
        {
            conn->R()->EnableReadWrite(conn->Sockfd(), true, false); // 关闭对写事件关心
        }
    }
    void HanlderExcpet(Connection *conn)
    {
        errno = 0;
        LOG(DEBUG, "client quit : %d", conn->Sockfd());
        conn->R()->RemoveConnection(conn->Sockfd());
    }

private:
    func_t _func;
};
#include <iostream>
#include <memory>

#include "Reactor.hpp"
#include "Listenner.hpp"
#include "Connection.hpp"
#include "Log.hpp"
#include "HandlerConnection.hpp"
#include "PackageParse.hpp"

// ./selectserver port
int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cout << "Usage: " << argv[0] << " port" << std::endl;
    return 0;
  }
  uint16_t port = std::stoi(argv[1]);

  EnableScreen();
  std::unique_ptr<Reactor> react = std::make_unique<Reactor>(); // 主服务

  HandlerConnection hc(PackageParse::Parse); // IO通信无关协议，回调上层业务处理
  Listenner listenner(port, hc);             // 负责连接的模块

  react->AddConnection(listenner.Sockfd(), EPOLLIN | EPOLLET,
                       std::bind(&Listenner::Accepter, &listenner, std::placeholders::_1), nullptr, nullptr); // 注册进入
  react->Dispatcher();

  return 0;
}
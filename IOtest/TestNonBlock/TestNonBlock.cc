#include <iostream>
#include <unistd.h>
#include "Comm.hpp"

int main()
{
  char buffer[1024];
  SetNonBlock(0); // 将0设置为非阻塞
  while (true)
  {
    ssize_t s = ::read(0, buffer, sizeof(buffer) - 1);
    if (s > 0)
    {
      buffer[s] = 0;
      std::cout << "Echo# " << buffer << std::endl;
    }
    else
    {
      // 问题:我怎么知道是底层IO条件不就绪，还是读取错误了呢？？？
      //  底层IO条件就绪和读取错误采用的是同样返回值操作的
      if (errno == EWOULDBLOCK || errno == EAGAIN)
      {
        std::cout << "底层数据没有就绪, 下次在试试吧! 做做其他事情！" << std::endl;
        sleep(1);
        continue;
      }
      else if (errno == EINTR)
      {
        continue;
      }
      else
      {
        std::cout << "读取错误... s : " << s << " errno: " << errno << std::endl;
        sleep(1);
      }
    }
  }
}
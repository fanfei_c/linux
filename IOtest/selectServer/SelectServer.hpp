#pragma once

#include <iostream>
#include <string>
#include <memory>
#include "Socket.hpp"

using namespace socket_ns;

class SelectServer
{
  const static int N = sizeof(fd_set) * 8;
  const static int defaultfd = -1;

public:
  SelectServer(uint16_t port)
      : _port(port), _listensock(std::make_unique<TcpSocket>())
  {
    InetAddr addr("0", _port);
    _listensock->BuildListenSocket(addr);
    for (int i = 0; i < N; i++)
    {
      _fd_array[i] = defaultfd;
    }
    // listensocket 等待新连接到来，等价于对方给我发送数据！我们作为读事件统一处理
    // 新连接到来 等价于 读事件就绪！
    // 首先要将listensock添加到select中！
    _fd_array[0] = _listensock->SockFd(); // 首先将listen套接字放入辅助数组
  }
  void AcceptClient()
  {
    InetAddr clientaddr;
    int sockfd = _listensock->Accepter(&clientaddr); // 这里调用accept会不会阻塞呢？？不会。因为事件已经就绪了
    if (sockfd < 0)
      return;

    LOG(DEBUG, "Get new Link, sockfd: %d, client info %s:%d\n", sockfd, clientaddr.Ip().c_str(), clientaddr.Port());
    int pos = 1;
    for (; pos < N; pos++)
    {
      if (_fd_array[pos] == defaultfd)
        break;
    }
    if (pos == N)
    {
      ::close(sockfd); // sockfd->Close();
      LOG(WARNING, "server is full!\n");
      return;
    }
    else
    {
      _fd_array[pos] = sockfd;
      LOG(DEBUG, "%d add to select array!\n", sockfd);
    }
    LOG(DEBUG, "curr fd_array[] fd list : %s\n", RfdsToString().c_str());
  }

  void ServiceIO(int pos)
  {
    char buffer[1024];
    ssize_t n = ::recv(_fd_array[pos], buffer, sizeof(buffer) - 1, 0); // 这里读取会不会被阻塞？不会
    if (n > 0)
    {
      buffer[n] = 0;
      std::cout << "client say# " << buffer << std::endl;
      std::string echo_string = "[server echo]# ";
      echo_string += buffer;
      ::send(_fd_array[pos], echo_string.c_str(), echo_string.size(), 0);
    }
    else if (n == 0)
    {
      LOG(DEBUG, "%d is closed\n", _fd_array[pos]);
      ::close(_fd_array[pos]);
      _fd_array[pos] = defaultfd;
      LOG(DEBUG, "curr fd_array[] fd list : %s\n", RfdsToString().c_str());
    }
    else
    {
      LOG(DEBUG, "%d recv error\n", _fd_array[pos]);
      ::close(_fd_array[pos]);
      _fd_array[pos] = defaultfd;
      LOG(DEBUG, "curr fd_array[] fd list : %s\n", RfdsToString().c_str());
    }
  }
  void HandlerEvent(fd_set &rfds)
  {
    for (int i = 0; i < N; i++)
    {
      if (_fd_array[i] == defaultfd)
        continue;
      if (FD_ISSET(_fd_array[i], &rfds))
      {
        if (_fd_array[i] == _listensock->SockFd()) // 如果是listen套接字则获取连接
        {
          AcceptClient();
        }
        else
        {
          ServiceIO(i); // 如果是普通套接字提供IO服务
        }
      }
    }
  }
  void Loop()
  {
    while (true)
    {
      fd_set rfds;
      FD_ZERO(&rfds); // 将文件描述符集清空
      int max_fd = defaultfd;

      for (int i = 0; i < N; i++)
      {
        if (_fd_array[i] == defaultfd)
        {
          continue;
        }
        FD_SET(_fd_array[i], &rfds); // 将所有合法的fd添加到rfds中
        if (max_fd < _fd_array[i])
        {
          max_fd = _fd_array[i]; // 更新出最大的fd的值
        }
      }

      struct timeval timeout = {0, 0};

      // select 同时等待的fd，是有上限的。因为fd_set是具体的数据类型，有自己的大小！
      // rfds是一个输入输出型参数，每次调用，都要对rfds进行重新设定!
      int n = select(max_fd + 1, &rfds, nullptr, nullptr, /*&timeout*/ nullptr);
      switch (n)
      {
      case 0:
        LOG(INFO, "timeout, %d.%d\n", timeout.tv_sec, timeout.tv_usec);
        break;
      case -1:
        LOG(ERROR, "select error...\n");
        break;
      default:
        LOG(DEBUG, "Event Happen. n : %d\n", n); // 底层有一个事件就绪，select为什么会一直通知我？因为：我们没有处理！
        HandlerEvent(rfds);
        break;
      }
    }
  }
  std::string RfdsToString()
  {
    std::string fdstr;
    for (int i = 0; i < N; i++)
    {
      if (_fd_array[i] == defaultfd)
        continue;
      fdstr += std::to_string(_fd_array[i]);
      fdstr += " ";
    }
    return fdstr;
  }
  ~SelectServer() {}

private:
  uint16_t _port;
  std::unique_ptr<Socket> _listensock;
  int _fd_array[N]; // 辅助数组
};
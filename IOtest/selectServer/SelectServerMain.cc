#include "SelectServer.hpp"
#include <memory>

// ./selectserver port
int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cout << "Usage: " << argv[0] << " port" << std::endl;
    return 0;
  }
  uint16_t port = std::stoi(argv[1]);

  EnableScreen();
  std::unique_ptr<SelectServer> svr = std::make_unique<SelectServer>(port);
  svr->Loop();

  return 0;
}
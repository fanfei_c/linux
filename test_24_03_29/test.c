#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void writer(int wfd)
{
  const char *str = "I am father";
  char buffer[128];
  int cnt = 0;
  pid_t pid = getpid();
  while (1)
  {
    snprintf(buffer, sizeof(buffer), "%s", str);
    write(wfd, buffer, strlen(buffer));
    sleep(1);
  }
}

void reader(int rfd)
{
  char buffer[1024];
  while (1)
  {
    ssize_t n = read(rfd, buffer, sizeof(buffer) - 1);
    (void)n;
    printf("child get a message from father: %s\n", buffer);
  }
}

int main()
{
  // 1.
  int pipefd[2];
  int n = pipe(pipefd);
  if (n < 0)
    return 1;
  printf("pipefd[0]: %d, pipefd[1]: %d\n", pipefd[0] /*read*/, pipefd[1] /*write*/); // 3, 4

  // 2.
  pid_t id = fork();
  if (id == 0)
  {
    // child: r
    close(pipefd[1]);

    reader(pipefd[0]);

    exit(0);
  }
  // father: w
  close(pipefd[0]);

  writer(pipefd[1]);
  wait(NULL);

  return 0;
}

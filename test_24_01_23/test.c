#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
int main()
{
	umask(0);
	int fd1 = open("log1.txt", O_RDONLY | O_CREAT, 0666);
	int fd2 = open("log2.txt", O_RDONLY | O_CREAT, 0666);
	int fd3 = open("log3.txt", O_RDONLY | O_CREAT, 0666);
	int fd4 = open("log4.txt", O_RDONLY | O_CREAT, 0666);
	int fd5 = open("log5.txt", O_RDONLY | O_CREAT, 0666);
	printf("fd1:%d\n", fd1);
	printf("fd2:%d\n", fd2);
	printf("fd3:%d\n", fd3);
	printf("fd4:%d\n", fd4);
	printf("fd5:%d\n", fd5);
	return 0;
}
//int main()
//{
//	FILE* fp = fopen("log.txt", "w");
//	if (fp == NULL){
//		perror("fopen");
//		return 1;
//	}
//	int count = 5;
//	while (count){
//		fputs("hello world\n", fp);
//		count--;
//	}
//	fclose(fp);
//	return 0;
//}
//#include<stdio.h>
//
//#define ONE 1
//#define TWO (1<<1)
//#define THREE (1<<2)
//#define FOUR (1<<3)
//#define FIVE (1<<4)
//
//void Print(int flag)
//{
//    if(flag & ONE) printf("1\n");
//    if(flag & TWO) printf("2\n");
//    if(flag & THREE) printf("3\n");
//    if(flag & FOUR) printf("4\n");
//    if(flag & FIVE) printf("5\n");
//}
//
//int main()
//{
//    Print(ONE);
//    printf("----------------------\n");
//    Print(TWO);
//    printf("----------------------\n");
//    Print(ONE|TWO);
//    printf("----------------------\n");
//    Print(THREE|FOUR|FIVE);
//    printf("----------------------\n");
//    Print(ONE|TWO|THREE|FOUR|FIVE);
//}

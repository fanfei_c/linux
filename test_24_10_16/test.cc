// #include <stdio.h>
// #include <stdlib.h>
// #include <unistd.h>
// #include <sys/types.h>
// #include <sys/wait.h>
// using namespace std;

// int main(int argc, char *argv[])
// {
//     int i;
//     printf("\tpid\t ppid \t pgid\n");
//     printf("parent\t%d\t%d\t%d\n", getpid(), getppid(), getpgid(getpid()));
//     for (i = 0; i < 2; i++)
//     {
//         if (fork() == 0)
//         {
//             printf("child\t%d\t%d\t%d\n", getpid(), getppid(), getpgid(getpid()));
//             return 0;
//         }
//     }
//     // 父进程等待所有子进程结束
//     for (i = 0; i < 2; i++)
//     {
//         wait(NULL); // 等待任意一个子进程结束
//     }
//     return 0;
// }

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

int main(int argc, char *argv[])
{
    int i;
    printf("\tpid\t ppid \t pgid\n");
    printf("parent\t%d\t%d\t%d\n", getpid(), getppid(), getpgid(getpid()));
    for (i = 0; i < 2; i++)
    {
        if (i == 0 && fork() == 0)
        {
            execlp("ls", "ls", "-l", NULL);
            return 0;
        }
        if (i == 1 && fork() == 0)
        {
            sleep(5);
            return 0;
        }
        wait(NULL);
    }

    // // 父进程等待所有子进程结束
    // for (int i = 0; i < 2; i++)
    // {
    //     wait(NULL); // 等待任意一个子进程结束
    // }
    return 0;
}

#include "Thread.hpp"
#include "ThreadPool.hpp"
#include "Log.hpp"
#include "Task.hpp"
#include <vector>
#include <string>
#include <memory>
#include <ctime>

using namespace ThreadModule;

int main()
{
  LOG(DEBUG, "程序加载完成");
  sleep(3);
  ThreadPool<Task>::GetInstance();
  sleep(2);
  ThreadPool<Task>::GetInstance();
  sleep(2);

  ThreadPool<Task>::GetInstance()->Stop();
  LOG(DEBUG, "终止线程池");
  sleep(2);
  ThreadPool<Task>::GetInstance()->Wait();
  LOG(DEBUG, "回收线程");
  sleep(2);

  // srand(time(nullptr) ^ getpid() ^ pthread_self());
  // EnableScreen(); // 开启日志显示器打印功能
  // // EnableFile();
  // std::unique_ptr<ThreadPool<Task>> tp = std::make_unique<ThreadPool<Task>>();
  // tp->initThreadPool();
  // tp->Start();

  // int taskNum = 10;
  // while (taskNum)
  // {
  //   int a = rand() % 10 + 1;
  //   usleep(1234);
  //   int b = rand() % 5 + 1;
  //   Task t(a, b);
  //   LOG(INFO, "main thread push task:%s", t.DebugToString().c_str());
  //   tp->Enqueue(t);
  //   sleep(1);
  //   taskNum--;
  // }

  // tp->Stop();
  // tp->Wait();
  // EnableScreen();
  // // EnableFile();
  // LOG(DEBUG, "hello");
  // LOG(DEBUG, "hello,%d", 10);
  // LOG(DEBUG, "hello,%d,%f", 10, 3.14);
  // std::unique_ptr<ThreadPool<int>> tp = std::make_unique<ThreadPool<int>>();
  // tp->initThreadPool();
  // tp->Start();
  // // tp->Enqueue();
  // sleep(2);
  // tp->Stop();
  // tp->Wait();
  return 0;
}
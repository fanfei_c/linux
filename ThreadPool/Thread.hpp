#ifndef __THREAD_HPP__
#define __THREAD_HPP__

#include <iostream>
#include <pthread.h>
#include <string>
#include <functional>
#include <unistd.h>

namespace ThreadModule
{
  // template <typename T>
  using func_t = std::function<void(std::string)>; // using 新语法相当于typedef

  // template <typename T>
  class Thread
  {
  public:
    void Excute()
    {
      _func(_threadname);
    }

  public:
    Thread(func_t func, const std::string &name = "none-name")
        : _func(func), _threadname(name), _stop(true)
    {
    }
    static void *threadroutine(void *args) // 类成员函数，形参是有this指针的，所以必须设置为静态成员函数
    {
      // _func(_data); // 要访问成员变量需要this指针，可是该函数为静态
      // 以下为解决方案
      Thread *self = static_cast<Thread *>(args); // this指针
      self->Excute();
      return nullptr;
    }
    bool Start()
    {
      int n = pthread_create(&_tid, nullptr, threadroutine, this); // 传入this指针
      if (!n)
      {
        _stop = false;
        return true;
      }
      else
        return false;
    }
    void Detach()
    {
      if (!_stop)
        pthread_detach(_tid);
    }
    void Join()
    {
      if (!_stop)
      {
        pthread_join(_tid, nullptr);
      }
    }
    std::string name()
    {
      return _threadname;
    }
    void Stop()
    {
      _stop = true;
    }
    ~Thread() {}

  private:
    pthread_t _tid;
    std::string _threadname;
    func_t _func;
    bool _stop;
  };
} // namespace ThreadModule

#endif
//#include <stdio.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <fcntl.h>
//#include <unistd.h>
//#include <string.h>

//int main()
//{
//    int fd = open("log.txt", O_WRONLY|O_CREAT|O_TRUNC, 0666);
//    dup2(fd, 1);
//    printf("hello linux\n");
////    close(1);
////    open("log.txt", O_WRONLY|O_CREAT|O_TRUNC, 0666);
////    printf("hello linux\n");
//}
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main()
{
    // 使用system call
    const char *s1 = "hello write\n";
    write(1, s1, strlen(s1));

    // 使用C语言接口
    const char *s2 = "hello fprintf\n";
    fprintf(stdout, "%s", s2);

    const char *s3 = "hello fwrite\n";
    fwrite(s3, strlen(s3), 1, stdout);


    fork();
    return 0;
}

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
static int thread2_result, thread1_count;

// // 进行计数
// void *thread1_routine(void *arg)
// {
// 	while (1)
// 	{
// 		thread1_count++; // 修改传入的整数值（注意：这仍然不是线程安全的）
// 		// sleep(1);
// 	}
// 	pthread_exit(NULL); // 线程正常退出
// }

// 进行计数
void *thread1_routine(void *arg)
{
	while (1)
	{
		(*(int*)arg)++; // 修改传入的整数值（注意：这仍然不是线程安全的）
		// sleep(1);
	}
	pthread_exit(NULL); // 线程正常退出
}

// 进行1-1000的累加
void *thread2_routine(void *arg)
{
	int ret = 0;
	for (int i = 1; i <= 1000; i++)
	{
		ret += i;
	}
	thread2_result = ret;
	// sleep(4);
	pthread_exit(NULL); // 线程正常退出
}

int main()
{
	pthread_t pid1, pid2;
	// int count=0;
	// int *count_ptr=&count;
	int* count=(int*)malloc(sizeof(int));
	if (pthread_create(&pid1, NULL, thread1_routine, count))
	{
		perror("pthread_create1 error");
		return 1;
	}
	if (pthread_create(&pid2, NULL, thread2_routine, NULL))
	{
		perror("pthread_create2 error");
		return 1;
	}
	void *result = NULL;
	pthread_join(pid2, &result);
	printf("thread2 result:%d\n", thread2_result);
	pthread_cancel(pid1);
	printf("thread1 count:%d\n", *count);
	free(count);
	// printf("thread1 count:%d\n", thread1_count);
	return 0;
}

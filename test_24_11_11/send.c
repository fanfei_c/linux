#include <stdio.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>

// 要求：发送进程接受用户提供的文件名字，将文件中的内容复制，通过共享内存接受进程，接受进程读取内容，并存储到参数提供的文件中。
// 提示：send.c和receive.c编译后按如下格式运行
// gcc send.c –o send
// gcc receive.c –o receive
// ./send a(a为存在的有内容的文件)
// ./receive b（b若不存在创建，复制时，若原来有内容，则覆盖）

int main(int argc, char *argv[])
{
	key_t key, key1;
	int id, semid;
	char *ptr;
	struct sembuf sops[6];
	
	key = ftok("/home/ff/code/linux/test_24_11_11", 'a');
	key1 = ftok("/", 'b');
	semid = semget(key1, 3, IPC_CREAT | 0666);
	id = shmget(key, 1024, IPC_CREAT | 0666);
	ptr = (char *)shmat(id, NULL, 0);
	// set sems original value
	union semun
	{
		int value;
		struct semid_ds *buf;
		unsigned short *array;
	} arg;
	short arr[3] = {0, 1, 1};
	arg.array = arr;
	semctl(semid, 0, SETALL, arg);
	// P(pronum)
	sops[0].sem_num = 0;
	sops[0].sem_op = -1;
	sops[0].sem_flg = SEM_UNDO;
	// P(space)
	sops[1].sem_num = 1;
	sops[1].sem_op = -1;
	sops[1].sem_flg = SEM_UNDO;
	// P(lock)
	sops[2].sem_num = 2;
	sops[2].sem_op = -1;
	sops[2].sem_flg = SEM_UNDO;
	// V(pronum)
	sops[3].sem_num = 0;
	sops[3].sem_op = 1;
	sops[3].sem_flg = SEM_UNDO;
	// V(space)
	sops[4].sem_num = 1;
	sops[4].sem_op = 1;
	sops[4].sem_flg = SEM_UNDO;
	// V(lock)
	sops[5].sem_num = 2;
	sops[5].sem_op = 1;
	sops[5].sem_flg = SEM_UNDO;

	int fd = open(argv[1], O_RDONLY);
	int cnt = -1;
	char buffer[1024] = {0};
	while (1)
	{
		// P(space);
		semop(semid, &sops[1], 1);
		// P(lock);
		semop(semid, &sops[2], 1);
		if ((cnt = read(fd, buffer, sizeof(buffer) - 1)) > 0) // 读取文件内容
		{
			buffer[1023] = '\0';
			strncpy(ptr, buffer, cnt + 1); // 向共享内存中写入
			bzero(buffer, sizeof(buffer));
		}
		else if (cnt == 0)
		{
			strncpy(ptr, "end\0", 5);
			break;
		}
		// V(lock);
		semop(semid, &sops[5], 1);
		// V(pronum);
		semop(semid, &sops[3], 1);	
	}
	close(fd);
	shmdt(ptr);
	printf("文件发送完毕\n");

	semctl(semid, 0, IPC_RMID);
}
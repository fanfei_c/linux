#pragma once

#include <iostream>

#include <functional>

using Task = std::function<void()>;

void Download()
{
  std::cout << "this is a download task" << std::endl;
}

// class Task
// {
// private:
//   int _a;
//   int _b;
//   int _result;

// public:
//   Task() {}
//   Task(int a, int b) : _a(a), _b(b), _result(0)
//   {
//   }
//   void Excute()
//   {
//     _result = _a + _b;
//   }
//   std::string ResultToString()
//   {
//     return std::to_string(_a) + "+" + std::to_string(_b) + "=" + std::to_string(_result);
//   }
//   std::string DebugToString()
//   {
//     return std::to_string(_a) + "+" + std::to_string(_b) + "=?";
//   }
//   ~Task() {}
// };
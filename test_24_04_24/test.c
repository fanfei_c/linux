#include <stdio.h>
#include <signal.h>

volatile int g_flag = 0;

void changeFlag(int signo)
{
  (void)signo;
  printf("将g_flag从%d->%d\n", g_flag, 1);
  g_flag = 1;
}

int main()
{
  signal(SIGINT, changeFlag);

  while (!g_flag)
    ;

  printf("process quit normal\n");
  return 0;
}

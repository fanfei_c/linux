#include <iostream>
#include <signal.h>
#include <unistd.h>

void Print(sigset_t &pending)
{
  std::cout << "curr process pending: ";
  for (int sig = 31; sig >= 1; sig--)
  {
    if (sigismember(&pending, sig))
      std::cout << "1";
    else
      std::cout << "0";
  }
  std::cout << "\n";
}

void handler(int signo)
{
  std::cout << "signal : " << signo << std::endl;
  // 不断获取当前进程的pending信号集合并打印
  sigset_t pending;
  sigemptyset(&pending);
  while (true)
  {
    sigpending(&pending);
    Print(pending);
    sleep(1);
  }
}

int main()
{
  struct sigaction act, oact;
  act.sa_handler = handler;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  sigaddset(&act.sa_mask, 3);
  sigaddset(&act.sa_mask, 4);
  sigaddset(&act.sa_mask, 5);

  sigaction(2, &act, &oact);

  while (true)
    sleep(1);

  return 0;
}

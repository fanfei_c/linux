#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>

const char *spaces = "     ";
const char *content1 = "Beginning Linux Programming";
const char *content2 = "Third Edition";
const char *filename = "./work/systemfile";

// int main()
// {
//     umask(0);
//     int fd = open(filename, O_RDWR | O_CREAT, 0640);
//     if (fd < 0)
//     {
//         perror("Error opening file systemfile");
//         return EXIT_FAILURE;
//     }

//     if (write(fd, content1, strlen(content1)) < 0)
//     {
//         perror("Error writing to file");
//         close(fd);
//         return EXIT_FAILURE;
//     }

//     off_t filesize;
//     if ((filesize = lseek(fd, 0, SEEK_END)) < 0)
//     {
//         perror("Error seeking to end of file");
//         close(fd);
//         return EXIT_FAILURE;
//     }

//     off_t offset = filesize - 10;
//     if (offset < 0)
//     { // 如果文件小于10个字节，则不能执行此操作
//         fprintf(stderr, "File is too small to perform the operation\n");
//         close(fd);
//         return EXIT_FAILURE;
//     }

//     // 移动读写指针到指定位置
//     if (lseek(fd, offset, SEEK_SET) < 0)
//     {
//         perror("Error seeking in file");
//         close(fd);
//         return EXIT_FAILURE;
//     }

//     // 写入五个空格到指定位置
//     if (write(fd, spaces, strlen(spaces)) < 0)
//     {
//         perror("Error writing spaces to file");
//         close(fd);
//         return EXIT_FAILURE;
//     }

//     close(fd);

//     int fd1 = open(filename, O_APPEND | O_RDWR, 0640);
//     if (fd1 < 0)
//     {
//         perror("Error opening file systemfile");
//         return EXIT_FAILURE;
//     }

//     if (write(fd1, content2, strlen(content2)) < 0)
//     {
//         perror("Error writing to file");
//         close(fd1);
//         return EXIT_FAILURE;
//     }

//     return 0;
// }

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void print_flags(int flags)
{
    printf("File flags: ");
    if (flags & O_RDONLY)
    {
        printf("O_RDONLY ");
    }
    if (flags & O_WRONLY)
    {
        printf("O_WRONLY ");
    }
    if (flags & O_RDWR)
    {
        printf("O_RDWR ");
    }
    if (flags & O_NONBLOCK)
    {
        printf("O_NONBLOCK ");
    }
    if (flags & O_APPEND)
    {
        printf("O_APPEND ");
    }
    printf("\n");
}

int main()
{
    int fd = open(filename, O_WRONLY | O_CREAT, 0644); // 以写模式和创建模式打开文件
    if (fd < 0)
    {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    // 获取当前文件状态标志
    int flags = fcntl(fd, F_GETFL);
    if (flags < 0)
    {
        perror("Error getting file flags");
        close(fd);
        return EXIT_FAILURE;
    }

    // 打印当前文件状态标志
    printf("Current file flags\n");
    print_flags(flags);

    flags |= O_APPEND;
    if (fcntl(fd, F_SETFL, flags) < 0)
    {
        perror("Error setting file flags");
        close(fd);
        return EXIT_FAILURE;
    }

    // 重新获取文件状态标志以确认设置
    flags = fcntl(fd, F_GETFL);
    if (flags < 0)
    {
        perror("Error getting file flags after set");
        close(fd);
        return EXIT_FAILURE;
    }
    // 打印设置后的文件状态标志
    printf("after setting O_APPEND\n");
    print_flags(flags);
    close(fd);

    return EXIT_SUCCESS;
}
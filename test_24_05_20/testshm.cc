#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <string>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace std;

const char *pathname = "/home/ff/linux/test_24_05_20";

const int proj_id = 0x66;

const int defaultsize = 4096;

int main()
{
  key_t key = ftok(pathname, proj_id);
  std::string s("i am processA");
  pid_t n = fork();
  if (n == 0)
  {
    int shmid = shmget(key, defaultsize, IPC_CREAT);
    char *addr = (char *)shmat(shmid, nullptr, 0);
    memset(addr, 0, defaultsize);
    strncpy(addr, s.c_str(), s.size() + 1);
    shmdt(addr);
  }
  wait(nullptr);
  int shmid = shmget(key, defaultsize, IPC_CREAT);
  char *addr = (char *)shmat(shmid, nullptr, 0);
  cout << "shm content: " << addr << std::endl;
  shmdt(addr);
  shmctl(shmid, IPC_RMID, nullptr);

  return 0;
}
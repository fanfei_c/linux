#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <unistd.h>
#include <cstring>
#include <pthread.h>
#include <functional>
#include "InetAddr.hpp"
#include "Log.hpp"

enum
{
  SOCKET_ERROR = 1,
  BIND_ERROR,
  LISTEN_ERROR,
  USAGE_ERROR
};

const static int sockfddefault = -1;
const static int gbacklog = 16;

class TcpServer; // 声明

class ThreadData
{
public:
  ThreadData(int fd, InetAddr addr, TcpServer *s) : sockfd(fd), clientaddr(addr), self(s)
  {
  }

public:
  int sockfd;
  InetAddr clientaddr;
  TcpServer *self;
};

using func_t = std::function<std::string(const std::string &)>;

class TcpServer
{
public:
  TcpServer(int port, func_t func) : _port(port), _listensockfd(sockfddefault), _isrunning(false), _func(func)
  {
  }
  void InitServer()
  {
    // 1. 创建流式套接字
    _listensockfd = ::socket(AF_INET, SOCK_STREAM, 0);
    if (_listensockfd < 0)
    {
      LOG(FATAL, "socket error");
      exit(SOCKET_ERROR);
    }
    LOG(DEBUG, "socket create success,sockfd is : %d", _listensockfd);

    // 2. 绑定
    struct sockaddr_in local;      // struct sockaddr_in 系统提供的数据类型。local是变量，用户栈上开辟空间。
    bzero(&local, sizeof(local));  // 将从&local开始的sizeof(local)大小的内存区域置零
    local.sin_family = AF_INET;    // 设置网络通信方式
    local.sin_port = htons(_port); // port要经过网络传输给对面，所有需要从主机序列转换为网络序列
    local.sin_addr.s_addr = INADDR_ANY;

    int n = bind(_listensockfd, (struct sockaddr *)&local, sizeof(local));
    if (n < 0)
    {
      LOG(FATAL, "bind error");
      exit(BIND_ERROR);
    }
    LOG(DEBUG, "bind success,sockfd is : %d", _listensockfd);

    // 3. tcp是面向连接的，所以通信之前，必须先建立连接，服务器是被链接的
    //  tcpserver启动，未来首先要一直等待客户端的连接，listen
    n = listen(_listensockfd, gbacklog);
    if (n < 0)
    {
      LOG(FATAL, "listen error");
      exit(LISTEN_ERROR);
    }
    LOG(DEBUG, "listen success,sockfd is : %d", _listensockfd);
  }
  void Service(int sockfd, InetAddr client)
  {
    LOG(DEBUG, "get a new link ,info %s:%d,fd:%d", client.Ip(), client.Port(), sockfd);
    std::string clientaddr = "[" + client.Ip() + ":" + std::to_string(client.Port()) + "]#";
    while (true)
    {
      // tcp连接面向字节流，可以使用文件接口：read，write
      char inbuffer[1024];
      // 1. tcp面向字节流，不能保证inbuffer是一个完整的命令字符串，以ls -a -l为例，一个完整的命令有可能只读了一半就执行了，即服务器可能不能完整的读到一个请求
      // 而udp不会出现这样的问题，因为udp面向数据报，收到则一定是完整的
      // 所以对于tcp，我们必须要在应用层面上保证我们收到的是一个完整的请求
      ssize_t n = recv(sockfd, inbuffer, sizeof(inbuffer) - 1, 0);
      if (n > 0)
      {
        inbuffer[n] = 0;
        std::cout << clientaddr << inbuffer << std::endl;
        std::string result = _func(inbuffer);
        send(sockfd, result.c_str(), result.size(), 0);
      }
      else if (n == 0) // read返回值如果为0，表示读到了文件结尾，即client退出&&关闭连接了
      {
        LOG(INFO, "%s quit", clientaddr.c_str());
        break;
      }
      else
      {
        LOG(ERROR, "read error");
        break;
      }
    }
    close(sockfd); // 文件描述符泄露
  }
  static void *HandlerSock(void *args) // IO和业务进行解耦合
  {
    pthread_detach(pthread_self()); // 线程分离
    ThreadData *td = static_cast<ThreadData *>(args);
    // 需要调用Service函数，但是Service函数是类内函数，静态成员函数没有this指针无法调用，如何解决？
    // 将this指针设为ThreadData的类内成员，再通过这个this调用Service
    td->self->Service(td->sockfd, td->clientaddr);
    delete td;
    return nullptr;
  }
  void Loop()
  {
    _isrunning = true;
    // 4. 不能直接接收数据，先获取连接
    while (_isrunning)
    {
      struct sockaddr_in peer;
      socklen_t len = sizeof(peer);
      // accept会阻塞等待，直到有客户端连接
      int sockfd = ::accept(_listensockfd, (struct sockaddr *)&peer, &len);
      if (sockfd < 0)
      {
        LOG(WARNING, "accept error");
        continue; // 失败了就继续获取就行，不需要退出
      };

      // version 2 ：采用多线程
      pthread_t t;
      ThreadData *td = new ThreadData(sockfd, InetAddr(peer), this);
      pthread_create(&t, nullptr, HandlerSock, td);
    }
    _isrunning = false;
  }
  ~TcpServer()
  {
    if (_listensockfd > sockfddefault)
      close(_listensockfd);
  }

private:
  uint16_t _port;
  int _listensockfd;
  bool _isrunning;

  func_t _func;
};
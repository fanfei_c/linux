#include "TcpServer.hpp"
#include "CommandExcute.hpp"
#include <memory>

void Usage(std::string proc)
{
  std::cout << "Usage:\n\t" << proc << " local_port\n"
            << std::endl;
}
// ./tcpserver port
// 云服务器的port默认都是禁止访问的。云服务器放开端口8080 ~ 8085
int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    Usage(argv[0]);
    // exit(USAGE_ERROR);
    return 1;
  }
  EnableScreen();

  // 网络部分
  uint16_t port = std::stoi(argv[1]);
  Command cmd("./conf.txt");
  func_t func = std::bind(&Command::Excute, &cmd, std::placeholders::_1);
  std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(port, func); // C++14
  tsvr->InitServer();
  tsvr->Loop();

  return 0;
}
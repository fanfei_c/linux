#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <unistd.h>
#include <cstring>
#include <pthread.h>
#include <functional>
#include <memory>
#include "InetAddr.hpp"
#include "Log.hpp"
#include "Socket.hpp"

using namespace socket_ns;
class TcpServer; // 声明

using io_service_t = std::function<void(socket_sptr sockfd, InetAddr client)>;

class ThreadData
{
public:
  ThreadData(socket_sptr fd, InetAddr addr, TcpServer *s) : sockfd(fd), clientaddr(addr), self(s)
  {
  }

public:
  socket_sptr sockfd;
  InetAddr clientaddr;
  TcpServer *self;
};

class TcpServer
{
public:
  TcpServer(int port, io_service_t service)
      : _localaddr("0", port), _listensock(std::make_unique<TcpSocket>()), _service(service), _isrunning(false)
  {
    _listensock->BuildListenSocket(_localaddr);
  }

  static void *HandlerSock(void *args) // IO和业务进行解耦合
  {
    pthread_detach(pthread_self()); // 线程分离
    ThreadData *td = static_cast<ThreadData *>(args);
    // 需要调用Service函数，但是Service函数是类内函数，静态成员函数没有this指针无法调用，如何解决？
    // 将this指针设为ThreadData的类内成员，再通过这个this调用Service
    td->self->_service(td->sockfd, td->clientaddr);
    ::close(td->sockfd->SockFd()); // 文件描述符泄露
    delete td;
    return nullptr;
  }
  void Loop()
  {
    _isrunning = true;
    // 4. 不能直接接收数据，先获取连接
    while (_isrunning)
    {
      InetAddr peeraddr;
      socket_sptr normalsock = _listensock->Accepter(&peeraddr);
      if (normalsock == nullptr)
        continue;

      // version 2 ：采用多线程
      pthread_t t;
      ThreadData *td = new ThreadData(normalsock, peeraddr, this);
      pthread_create(&t, nullptr, HandlerSock, td);
    }
    _isrunning = false;
  }
  ~TcpServer()
  {
  }

private:
  InetAddr _localaddr;
  std::unique_ptr<Socket> _listensock;
  bool _isrunning;

  io_service_t _service;
};
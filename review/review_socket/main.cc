#include "threadpool_tcp.hpp"
#include <csignal>


// 信号处理器，用于优雅关闭服务器
std::atomic<bool> serverRunning(true);

void SignalHandler(int signo) {
    if (signo == SIGINT) {
        serverRunning = false;
    }
}

int main()
{
    signal(SIGINT, SignalHandler);
    TcpServer serv(8080,10,[](const std::string &req){return "echo: "+req;});
    serv.InitServer();
    while(serverRunning)
        serv.Loop();
    serv.Stop();
    std::cout << "Server stopped." << std::endl;
    return 0;
}
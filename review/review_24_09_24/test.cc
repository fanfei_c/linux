// #include <iostream>
// #include <unistd.h>
// #include <sys/types.h>
// #include <sys/stat.h>
// #include <fcntl.h>
// using namespace std;

// int main()
// {
//     umask(0);
//     // int fd = open("file.txt", O_RDONLY | O_CREAT, 0666);
//     int fd = open("file.txt", O_RDONLY);
//     close(fd);
//     return 0;
// }

// //程序1
// #include <stdio.h>

// int main()
// {
//     FILE *fp1 = fopen("./tmp/tmpfile", "w+");
//     fclose(fp1);
//     return 0;
// }

// //程序2

// #include <stdio.h>
// #include <string.h>

// int main()
// {
//     FILE *fp1 = fopen("./a", "r");
//     FILE *fp2 = fopen("./b", "w");
//     if (fp2 == NULL)
//     {
//         printf("Error opening file b");
//         return 1;
//     }
//     FILE *fp3 = fopen("./c", "a");

//     char buf[1024]{0};
//     size_t bytesRead;
//     while ((bytesRead = fread(buf, 1, sizeof(buf), fp1)) > 0)
//     {
//         fwrite(buf, 1, bytesRead, fp2);
//         fwrite(buf, 1, bytesRead, fp3);
//     }
//     fclose(fp1);
//     fclose(fp2);
//     fclose(fp3);
//     return 0;
// }

#include <stdio.h>
#include <string.h>

typedef struct student
{
    int year;
    char name[50];
    float score;
} stu;

int main()
{
    FILE *fp1 = fopen("./d", "w");
    if (fp1 == NULL)
    {
        perror("Error opening file b");
        return 1;
    }
    stu s[3]{{24, "张三", 90.5}, {24, "李四", 91.5}, {24, "王五", 92.5}};
    fwrite(s, sizeof(stu), 3, fp1);
    fclose(fp1);

    FILE *fp2 = fopen("./d", "r");
    stu student;
    while (fread(&student, sizeof(stu), 1, fp2) == 1)
    {
        printf("Year: %d, Name: %s, Score: %.2f\n", student.year, student.name, student.score);
    }
    fclose(fp2);
    return 0;
}
#include <iostream>

class Date
{
private:
    int year;
    int month;
    int day;

public:
    Date()
    {
    }
    ~Date()
    {
    }
    friend std::ostream &operator<<(std::ostream &out, const Date &date);
};
std::ostream &operator<<(std::ostream &out, const Date &date)
{
    out << date.year << ":" << date.month << ":" << date.day << std::endl;
    return out;
}
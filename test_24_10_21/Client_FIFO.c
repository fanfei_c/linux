#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main()
{
    char buffer[1024] = {0};
    if (access("fifo1", F_OK) == -1)
    {
        mkfifo("fifo1", 0644);
    }
    if (access("fifo2", F_OK) == -1)
    {
        mkfifo("fifo2", 0644);
    }
    // fifo1负责服务端向客户端发送，客户端读
    int fd1 = open("fifo1", O_RDONLY);
    int fd2 = open("fifo2", O_WRONLY);
    int cnt = 0;
    if (fd1 < 0 || fd2 < 0)
    {
        perror("file open fail");
        return 1;
    }
    int ret = fork();

    // 父进程读取服务端发送的数据
    if (ret > 0)
    {
        while (1)
        {
            cnt = read(fd1, buffer, sizeof(buffer) - 1);
            if (cnt > 0)
            {
                printf("Server Say:\n");
                write(1, buffer, cnt);
                printf("Client#:\n");
            }
        }
        close(fd1);
        wait(NULL);
    }
    // 子进程向服务端写，服务端读
    else if (ret == 0)
    {
        while (1)
        {
            printf("Client#:\n");
            cnt = read(0, buffer, sizeof(buffer) - 1);
            write(fd2, buffer, cnt);
        }
        close(fd2);
    }
    else
    {
        perror("fork");
        return 1;
    }
    return 0;
}
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
int main()
{
    int val;
    int data=10;
    if((val=fork())<0)
    {
        printf("fork erro");
        exit(1);
    }
    else if(val==0)
    {
        data--;
        printf("child's data is:%d\n",data);
        exit(0);
    }
    else{
        printf("father's data is:%d\n",data);
    }
    wait(NULL);
    return 0;
}

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main()
{
    pid_t pid=fork();
    if(pid<0)
    {
        printf("ERROR\n");
        exit(0);
    }
    else if(pid>0)
    {
        wait(0);
        pid_t pid2=fork();
        if(pid2<0)
        {
            printf("ERROR\n");
            exit(0);
        }
        else if(pid2>0)
        {
            wait(0);
            printf("i am father,pid:%d,ppid:%d\n",getpid(),getppid());
        }
        else{
            printf("i am daughter,pid:%d,ppid:%d\n",getpid(),getppid());
        }
    }
    else{
        printf("i am son,pid:%d,ppid:%d\n",getpid(),getppid());
    }
    return 0;
}

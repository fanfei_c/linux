#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
int main()
{
     pid_t pid1=1234;
     pid_t pid2=5678;
 
     pid1 = fork();
     pid2 = fork();
 
     printf("pid1=%d,pid2=%d\n",pid1,pid2);
     wait(NULL);
     wait(NULL);
     exit(0);
}


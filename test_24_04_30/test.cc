#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>

// void *newthreadrun(void *args)
// {
//   while (true)
//   {
//     std::cout << "I am new thread, pid: " << getpid() << " new thread id: " << pthread_self() << std::endl;
//     sleep(1);
//     std::cout << "new thread exit!" << std::endl;
//   }
// }

// int main()
// {
//   pthread_t tid;
//   pthread_create(&tid, nullptr, newthreadrun, nullptr);

//   while (true)
//   {
//     std::cout << "I am main thread, pid: " << getpid() << " main thread id: " << pthread_self() << std::endl;
//     std::cout << "I am main thread, pid: " << getpid() << " new thread id: " << tid << std::endl;
//     sleep(1);
//   }
// }

// #include <stdio.h>
// #include <stdlib.h>
// #include <pthread.h>
// #include <unistd.h>
// #include <sys/types.h>

// void *newthreadrun(void *args)
// {
//   int count = 5;
//   while (count > 0)
//   {
//     printf("I am new thread, pid: %d, ppid: %d, tid: %lu\n", getpid(), getppid(), pthread_self());
//     sleep(1);
//     count--;
//   }
//   return NULL;
// }
// int main()
// {
//   pthread_t tid;
//   pthread_create(&tid, nullptr, newthreadrun, nullptr);

//   printf("I am main thread...pid: %d, ppid: %d, tid: %lu\n", getpid(), getppid(), pthread_self());

//   pthread_join(tid, NULL);
//   printf("new thread quit\n");

//   return 0;
// }
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>

void *newthreadrun(void *args)
{
  int count = 5;
  while (count > 0)
  {
    printf("I am new thread, pid: %d, ppid: %d, tid: %lu\n", getpid(), getppid(), pthread_self());
    sleep(1);
    count--;
  }
  pthread_exit((void *)666);
}
int main()
{
  pthread_t tid;
  pthread_create(&tid, nullptr, newthreadrun, nullptr);

  printf("I am main thread...pid: %d, ppid: %d, tid: %lu\n", getpid(), getppid(), pthread_self());

  void *ret = nullptr;
  pthread_join(tid, &ret);
  printf("new thread quit,exitcode: %lld\n", (long long)ret);

  return 0;
}